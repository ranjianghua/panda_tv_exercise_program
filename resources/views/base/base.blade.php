<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="/css/reset.css">
	<link rel="stylesheet" href="/css/base.css">
	<script src="/js/jquery.min.js"></script>
	
</head>

<body style="height: 100%;">
	<div class="base-left">
		<!-- logo搜索  S -->
		<div class="logo">
			<img class="logo-img" src="/image/3459966f6e12c3f61e596278e5af4b54.webp" alt="">
		</div>

		<div class="search">
			<form action="" onsubmit="zwcgn()">
				<input onclick="s1(this)" onblur="s2(this)" id="search" type="text" class="search-bar" value="搜房间/搜主播">
				<input id="search-sub" type="submit" class="search-sub" value=" ">
			</form>
		</div>
		<div class="left-hr"></div>
		<!-- logo搜索  E -->
		<!-- 选项div  S -->
		<div class="left-content"

		@if(session('user_id')!=null)
		style="bottom:122px;"
		@endif 
		 >
			<!-- 大分类  S -->
			<div class="select-main">
				<a class="select s-qbzb" href="/fenlei">
					
					@if($menu=='qbzb')
					<span class="icon" style="background-position: -69px -12px;"></span> 
					<span class="select-name" style="color:#1cc692;font-weight: 800;">全部直播</span>
					@else
					<span class="icon"></span> 
					<span class="select-name">全部直播</span>
					@endif
					
				</a>
				<a class="select s-qbfl" href="/">
					@if($menu=='qbfl')
					<span class="icon" style="background-position: -69px -52px;"></span> 
					<span class="select-name" style="color:#1cc692;font-weight: 800;">全部分类</span>
					@else
					<span class="icon"></span> 
					<span class="select-name">全部分类</span>
					@endif
				</a>
				<a class="select s-zbph" href="/zbph">
					@if($menu=='zbph')
					<span class="icon" style="background-position: -69px -92px;"></span> 
					<span class="select-name" style="color:#1cc692;font-weight: 800;">主播排行</span>
					@else
					<span class="icon"></span> 
					<span class="select-name">主播排行</span>
					@endif
				</a>
			</div>
			<!-- 大分类  E -->
			<!-- 热门分类  S -->
			<div class="select-hot">
				<div class="biaoti">
					热门分类
				</div>
				<div class="select-main hot-main">
					<a class="select s-qbzb" href="/ejflzq/8/2">
						<span class="select-name"
							@if($menu=='jdqs-h')
							style="color:#1cc692;font-weight: 800"
							@endif
						>艺能达人</span>
						<span class="hot">HOT</span>
					</a>
					<a class="select s-qbzb" href="/ejflzq/4/1">
						<span class="select-name"
							@if($menu=='xmxx-h')
							style="color:#1cc692;font-weight: 800"
							@endif
						>英雄联盟</span>
						<span class="hot">HOT</span>
					</a>
					<a class="select s-qbzb" href="/ejflzq/9/2">
						<span class="select-name"
							@if($menu=='blzy-h')
							style="color:#1cc692;font-weight: 800"
							@endif
						>美食</span>
						<span class="hot">HOT</span>
					</a>
				</div>
			</div>
			<!-- 热门分类  E -->	
			<!-- 全部分类  S -->
			<div class="select-hot bgfl">
				<div class="biaoti">
					全部分类
				</div>
				<!-- 单个分类包含展开包位置 S
					内需传id名3次
					如下：rmjj
				 -->
				<div class="big-fenlei">
					<a href="{{route('flzq',['id'=>1])}}"><div class="title">
						热门竞技
					</div></a>
					<div class="button" onclick="zhankai('#rmjj',this)">
						<a class="icon"></a>
						<a class="icon-wenzi" id="rmjj-wenzi">展开</a>
					</div>
				</div>
				<div class="xiangxi" id="rmjj">
				@foreach($type as $t)
					@if($t->pid==1)
					<a href="{{route('ejflzq',['id'=>$t->id,'pid'=>1])}}" class="xiangxixx">
						{{$t->type_name}}
					</a>
					@endif
				@endforeach	
				</div>
				<!-- 单个分类含展开  E -->		
				<!-- 单个分类包含展开包位置 S -->
				<div class="big-fenlei">
					<a href="{{route('flzq',['id'=>2])}}"><div class="title">
						娱乐联盟
					</div></a>
					<div class="button" onclick="zhankai('#yllm',this)">
						<a class="icon"></a>
						<a class="icon-wenzi" id="yllm-wenzi">展开</a>
					</div>
				</div>
				<div class="xiangxi" id="yllm">
				@foreach($type as $t)
					@if($t->pid==2)
					<a href="{{route('ejflzq',['id'=>$t->id,'pid'=>2])}}" class="xiangxixx">
					{{$t->type_name}}
					</a>
					@endif
				@endforeach	
				</div>
				<!-- 单个分类含展开  E -->
				<!-- 单个分类包含展开包位置 S -->
				<div class="big-fenlei">
					<a href="{{route('flzq',['id'=>3])}}"><div class="title">
						网游专区
					</div></a>
					<div class="button" onclick="zhankai('#syjq',this)">
						<a class="icon"></a>
						<a class="icon-wenzi" id="syjq-wenzi">展开</a>
					</div>
				</div>
				<div class="xiangxi" id="syjq">
				@foreach($type as $t)
					@if($t->pid==3)
					<a href="{{route('ejflzq',['id'=>$t->id,'pid'=>3])}}" class="xiangxixx">
					{{$t->type_name}}
					</a>
					@endif
				@endforeach
					
				</div>
				<!-- 单个分类含展开  E -->
			</div>
			<!-- 全部分类  E -->		
		</div>
		<!-- <div class="left-hr"></div> -->
		<!-- 选项div  E -->
		@if(session('user_id')==null)
		<div class="userinfo">
			
			<div class="denglu">
				<a onclick="tanchuang('login')" id="abcde" class="dl-button login" style="float: left;color: #1cd388;border-color: #1dd388;">登录</a>
				<a onclick="tanchuang('register')" id="wasd" class="dl-button register" style="float: right;color: #b2b4c4;border-color: #6e7081;">注册</a>
			</div>
		</div>
		@else
		<div class="userinfo" style="height: 118px;background-color: #3a394d;border:6px solid #191b2e;border-top: 0;border-bottom: 0;">
			<a href="/grzx">
				<div class="circle">
					@if(session('mdface')=="")
					<img src="/image/default-face.jpg" alt="">
					@else
					<img src="/uploads/{{session('mdface')}}" alt="">
					@endif
				</div>
			</a>
			<p class="username">{{session('nick_name')}}</p>
			<div class="duanwei"><i id="dw-icon"></i></div>
			<div class="huobi">
				<div class="hb-zhuzi">
					<i></i>
					<span>{{session('zhuzi')}}</span>
				</div>
				<div class="hb-maobi">
					<i></i>
					<span>{{session('maobi')}}</span>
				</div>
			</div>
			<div class="yincang">
				<a href="" class="chongzhimb">充值猫币</a>
				<p class="czsm">充值10元，并没有什么奖励</p>
				<div class="wydzb-border">
					
					@if(session('user_type')==1)
				 	<a class="wydzb">	
					暂不开放
					@else
					<a href="/grzx" class="wydzb">
					直播间管理
					@endif
					</a>
				</div>
				<div class="set-logout">
					<a href="/grzx" class="yc-set">
						<i></i>设置
					</a>
					<a href="{{route('logout')}}" class="yc-logout">
						<i></i>退出登录
					</a>
				</div>
			</div>
		</div>
		@endif
	</div>
	<div class="base-right">
		@yield('content')
	</div>
	<div id="mengban" style="z-index:10000;">
		<div class="tc-login tc-juzhong">
			<div class="tc-xx">
				<a href="javascript:;"  class="tc-xx-dl">登录</a>
				<a href="javascript:;" onclick="qhzc()" class="tc-xx-zc">注册</a>
				<div class="guangbi" onclick="guanbi()">
					
				</div>
			</div>
			<div class="tc-dl">
				<form action="{{route('login')}}" method="post">
					{{csrf_field()}}
					@if($errors->any())
					<ul class="error">
						@foreach($errors->all() as $e)
						<li>{{$e}}</li>
						@endforeach
					</ul>
					@endif
					<div class="phonenum">
						<span class="tb">
							<i></i>
						</span>
						<div class="phone-input">
							<span>+86</span>
							<input type="text" name="mobile" placeholder="请输入手机号">
						</div>
					</div>
					<div class="pwd">
						<span class="tb">
							<i></i>
						</span>
						<div class="pwd-input">
							<input type="password" name="password" placeholder="请输入密码">
						</div>
					</div>
					<input type="submit" value="立即登录" class="tc-ljdl">
				</form>
			</div>
		</div>
		<div class="tc-register tc-juzhong">
			<div class="tc-xx">
				<a href="javascript:;" onclick="qhdl()" class="tc-xx-dl">登录</a>
				<a href="javascript:;" class="tc-xx-zc">注册</a>
				<div class="guangbi" onclick="guanbi()">
					
				</div>
			</div>
			<div class="tc-dl">
				<form action="{{route('doregist')}}" method="post">
				
					{{csrf_field()}}
					@if($errors->any())
					<ul class="error">
						@foreach($errors->all() as $e)
						<li>{{$e}}</li>
						@endforeach
					</ul>
					@endif
					<div class="phonenum">
						<span class="tb">
							<i></i>
						</span>
						<div class="phone-input">
							<span>+86</span>
							<input type="text" name="mobile"
							placeholder="请输入手机号" maxlength="11">
						</div>
					</div>
					<div class="pwd yzm">
						<span class="tb">
							<i style="background-position: -4pc -49px;"></i>
						</span>
						<div class="pwd-input">
							<input type="text" name="mobile_code" placeholder="请输入验证码" maxlength="6">
							<!-- <input id="btn-send" type="button" value="发送验证码" class="ruc-send-auth-code-btn cn-click"></div> -->
							<a  id="btn-send" class="ruc-send-auth-code-btn disabled">获取验证码</a>
						</div>
					</div>

					<div class="pwd">
						<span class="tb">
							<i style="background-position: -75pt -50px;"></i>
						</span>
						<div class="pwd-input">
							<input type="text" name="nick_name" placeholder="请输入昵称" maxlength="12">
						</div>
					</div>
					<div class="pwd">
						<span class="tb">
							<i></i>
						</span>
						<div class="pwd-input">
							<input type="password" name="password" placeholder="请输入密码" maxlength="16">
						</div>
					</div>
					<input type="submit" value="注册" class="tc-ljdl">
				</form>
			</div>
		</div>
	</div>
</body>
</html>
<script>
	function zwcgn(){
		alert('暂无此功能');
	}
	function zwfl(){
		alert('暂无此分类');
	}
	@if(session('user_id')!=null)
	//个人信息弹起逻辑
	$('.userinfo').hover(function(){
		$(this).stop().animate({height:'323px'});
		$('.yincang').css('display','block');
	},function(){
		$(this).stop().animate({height:'118px'},600,function(){
			$('.yincang').css('display','none');
		});
	})

	function duanwei(){
		var duanwei = null;
		duanwei = {{session('duanwei')}};
		// duanwei = 8;
		if(duanwei!=null){
			var x = 0;
			var y = 0;
			switch(duanwei){
				case 1:x=-200;y=0;break;
				case 2:x=-150;y=0;break;
				case 3:x=-100;y=0;break;
				case 4:x=-50;y=0;break;
				case 5:x=-0;y=0;break;
				case 6:x=-200;y=-20;break;
				case 7:x=-150;y=-20;break;
				case 8:x=-100;y=-20;break;
				case 9:x=-50;y=-20;break;
				case 10:x=-0;y=-20;break;
				case 11:x=-200;y=-40;break;
				case 12:x=-150;y=-40;break;
				case 13:x=-100;y=-40;break;
				case 14:x=-50;y=-40;break;
				case 15:x=-0;y=-40;break;
				default:break;
			}
			$('#dw-icon').css('background-position',x+'px '+y+'px');
		}
	}
	
	duanwei();
	@endif

	@if(session('abc'))
		$('#abcde').click();
	@endif

	@if(session('aaa'))
		$('#wasd').click();
	@endif
	function s1(input){
		if($(input).val()=="搜房间/搜主播"){
			$(input).val('');
		}
		$(input).css('color','#c0c2ce');
		// $(input).removeAttr('onclick');
		$('#search-sub').css('background-position','-255px -74px');
	}
	function s2(input){
		if($(input).val()==""){
			$(input).val('搜房间/搜主播');
		}
		$(input).css('color','#6e7081');
		$('#search-sub').css('background-position','-224px -74px');
	}
	//下拉菜单展开逻辑
	function zhankai(name,a){
		$(name).slideDown();
		$(a).attr('onclick',"shouqi('"+name+"',this)");
		$(name+'-wenzi').html('收起');
	}

	//下拉菜单收起逻辑
	function shouqi(name,a){
		$(name).slideUp();
		$(a).attr('onclick','zhankai("'+name+'",this)');
		$(name+'-wenzi').html('展开');
	}

	//登录注册弹窗逻辑
	function tanchuang(a){
		$('#mengban').fadeIn();
		if(a=="login"){
			$('.tc-register').css('display','none');
			$('.tc-login').css('display','block');
		}else{
			$('.tc-login').css('display','none');
			$('.tc-register').css('display','block');
		}
	}

	function qhzc(){
		$('.tc-login').css('display','none');
		$('.tc-register').css('display','block');
	}
	function qhdl(){
		$('.tc-register').css('display','none');
		$('.tc-login').css('display','block');
	}

	function guanbi(){
		$('#mengban').fadeOut();
	}
</script>
<script>

$(function(){
	var seconds=60;
	var si;  // 保存定时器
	var flag = 1;
	$("#btn-send1").click(function(){

		if(flag==1){
		flag=0;
		// 获取手机号码
		var mobile = $("input[name=mobile]").val();

		// 执行AJAX发到服务器
			$.ajax({
				type:"GET",     // GET方式
				url:"{{ route('ajax-send-modbile-code') }}",    // AJAX提交的地址
				data:{mobile:mobile},                           // 提交的参数（手机号码）
				success:function(data) {                        // AJAX成功之后执行的代码

					// 设置按钮失效
					$("#btn-send").addClass("disabled");

					// 每1秒执行一次
					si = setInterval(function(){
						seconds--;
						flag = 1;
						if(seconds==0)
						{
							// 生效
							$("#btn-send").removeClass("disabled");
							seconds=60;
							$("#btn-send").html("获取验证码");
							// 关闭定时器
							clearInterval( si );
						}
						else{
							$("#btn-send").html("还剩："+(seconds));
						}
					}, 1000);
				}
			});
		}
	});
});

</script>