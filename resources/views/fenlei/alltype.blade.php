@extends('base.base') 

@section('title','全部分类')

@section('content')


<link rel="stylesheet" href="css/fenlei.css">
<script src="/js/bootstrap.min.js"></script>
    <div class="container-fluid">
        <div class="later-play-container">
          <!-- 列表头部信息 start -->
            <div class="list-header" data-pdt-block="a0">
                <i class="header-line"></i>
                <div class="list-title">全部分类</div>
                <ul class="list-header-hots clearfix">
                    <div class="btnys" id="btnys">
                        <li>
                            <a class="selected" href="javascript:;" title="全部" abc='0' data-cate="__all__" data-toggle="panda-monitor" data-paew="all-flbq-all">全部</a>
                        </li>
                        @foreach($type as $v)
                            @if($v->pid==0)
                            <li>
                                <a class="" href="javascript:;" target="_blank" abc='{{$v->id}}' title="" data-cate="jingji" data-toggle="panda-monitor" data-paew="pc_web-all-flbq_jingji">
                                    {{$v->type_name}}</a>
                            </li>
                            @endif
                        @endforeach
                       
                    </div>
                </ul>
            </div>
            <!-- 列表头部信息 end -->
            <div class="sort-container">
                <div class="tbton" style="display:block;">
                    <ul class="sort-menu video-list clearfix" data-pdt-block="s1"> 
                    @foreach($type as $t)  
                        @if($t->pid>0)              
                        <li class="video-list-item parent-jingji" abc='{{$t->pid}}' style="display: block;">
                            <a class="video-list-item-wrap"  href="{{route('ejflzq',['id'=>$t->id,'pid'=>$t->pid])}}" data-pdt-ele="3">
                                <div class="img-container">
                                <img src="uploads/{{$t->type_img}}" alt="">
                                </div>
                                <div class="cate-title">
                                {{ $t->type_name}}      
                                </div>
                            </a>
                        </li>
                        @endif
                    @endforeach                         
                    </ul>
                </div>
            </div>
        </div>
    </div>


<script>



    $(function(){   
        

       $("#btnys li").on("click",'a',function(){
         // 设index为当前点击
         var num = $("#btnys li a").index(this);
         var a = $(this).attr('abc');
         console.log(a);
        
         // 点击添加样式利用siblings清除其他兄弟节点样式
         $(this).addClass("selected").parent().siblings().find("a").removeClass("selected");
         // 同理显示与隐藏
         $('.video-list-item').css('display','none');
         $('.video-list [abc="'+a+'"]').css('display','block');
         if(a=="0"){
            $('.video-list-item').css('display','block');
        }
        
        
       });
    });




</script>
@endsection