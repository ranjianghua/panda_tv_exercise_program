@extends('base.base') 

@section('title','直播')

@section('content')
<link rel="stylesheet" href="/css/fenlei.css">
<script src="/js/bootstrap.min.js"></script>
    <div class="container-fluid">
    <div class="later-play-container">
          <!-- 列表头部信息 start -->
          <div class="list-header" data-pdt-block="a0">
            <i class="header-line"></i>
            <div class="list-title">正在直播</div>
            <ul class="list-header-hots clearfix">
                <div class="btnzb" id="btnzb">
                    <li>
                        <a class="selected" href="javascript:;" title="全部" data-cate="__all__" abc="0" data-toggle="panda-monitor" data-paew="all-flbq-all">全部</a>
                    </li>
                    @foreach($type as $v)
                        @if($v->pid==0)
                    <li>
                        <a class="" href="javascript:;" target="_blank" title="竞技游戏" abc="{{$v->id}}" data-cate="jingji" data-toggle="panda-monitor" data-paew="pc_web-all-flbq_jingji">
                        {{$v->type_name}}</a>
                    </li>
                        @endif
                    @endforeach
                    
                </div>
            </ul>
            <!-- <ul class="list-header-filter">
              <li class="filter-item" data-type="ticket" data-toggle="panda-monitor" data-paew="pc_web-all-filter_ticket">按车票</li>
              <li class="filter-item" data-type="person" data-toggle="panda-monitor" data-paew="pc_web-all-filter_person">按人气</li>
              <li class="filter-item selected" data-type="top" data-toggle="panda-monitor" data-paew="pc_web-all-filter_top">推荐</li>
            </ul> -->
          </div>
          <!-- 列表头部信息 end -->
          <div>
            <div class="zhibo" style="display:block;">
                <ul id="later-play-list" class="video-list clearfix" data-total="2443">  
                @foreach($lives as $l)
                    @if($l->live_state==1)
                    <li class="video-list-item1 video-no-tag  " abc="{{$l->pid}}" data-pdt-block="a_jingji1-0" data-id="237908" style="display:block;">
                        <a href="/live-{{$l->id}}" class="video-list-item-wrap" data-pdt-ele="0" data-id="237908">
                        <div class="video-cover">
                            <img class="video-img video-img-lazy" data-original="" alt="老刘：刘莱厄斯1V5Carry流！" src="/uploads/{{$l->cover}}" style="display: block;">
                            <div class="video-overlay"></div>
                            <div class="video-play"></div>
                        
                        </div>
                        <div class="video-info">
                            <span class="video-title" title="">{{$l->live_name}}</span>

                        </div>
                        </a>
                        <div class="video-label">
                        <div class="video-label-content">

                            <a class="video-label-item label-color-0" href="/ejflzq/{{$l->type_id}}/{{$l->pid}}" data-pdt-ele="1">{{$l->type_name}}</a>
                            <span class="video-title" style="float:right;margin-top:5px;font-size: 12px;color:#999;" title="">{{$l->zb_name}}</span>

                            
                        </div>
                        </div>
                    </li> 
                    @endif
                @endforeach
                </ul>
            </div>
            <!-- <div class="zhibo" style="display:none;">
                <ul id="later-play-list" class="video-list clearfix" data-total="2443">  
                    <li class="video-list-item1 video-no-tag  " data-pdt-block="a_jingji1-0" data-id="237908">
                        <a href="/237908" class="video-list-item-wrap" data-pdt-ele="0" data-id="237908">
                        <div class="video-cover">
                            <img class="video-img video-img-lazy" data-original="https://i.h2.pdim.gs/90/47d745af8e95ced57c22b83b5c464643/w338/h190.webp" alt="老刘：刘莱厄斯1V5Carry流！" src="https://i.h2.pdim.gs/90/05939e07a11f1fa12f2a3f2aef06ac6a/w338/h190.webp" style="display: block;">
                            <div class="video-overlay"></div>
                            <div class="video-play"></div>
                        
                        </div>
                        <div class="video-info">
                            <span class="video-title" title="老刘：刘莱厄斯1V5Carry流！">2老刘：刘莱厄斯1V5Carry流！</span>                              
                        </div>
                        </a>
                        <div class="video-label">
                        <div class="video-label-content">
                            <a class="video-label-item label-color-0" href="/cate/lol" data-pdt-ele="1">英雄联盟</a>
                            <a class="video-label-item label-color-5" href="/label/lmjy" data-pdt-ele="2">联盟精英</a>
                        </div>
                        </div>
                    </li> 
                </ul>
            </div>
            <div class="zhibo" style="display:none;">
                <ul id="later-play-list" class="video-list clearfix" data-total="2443">  
                    <li class="video-list-item1 video-no-tag  " data-pdt-block="a_jingji1-0" data-id="237908">
                        <a href="/237908" class="video-list-item-wrap" data-pdt-ele="0" data-id="237908">
                        <div class="video-cover">
                            <img class="video-img video-img-lazy" data-original="https://i.h2.pdim.gs/90/47d745af8e95ced57c22b83b5c464643/w338/h190.webp" alt="老刘：刘莱厄斯1V5Carry流！" src="https://i.h2.pdim.gs/90/05939e07a11f1fa12f2a3f2aef06ac6a/w338/h190.webp" style="display: block;">
                            <div class="video-overlay"></div>
                            <div class="video-play"></div>
                        
                        </div>
                        <div class="video-info">
                            <span class="video-title" title="老刘：刘莱厄斯1V5Carry流！">3老刘：刘莱厄斯1V5Carry流！</span>                              
                        </div>
                        </a>
                        <div class="video-label">
                        <div class="video-label-content">
                            <a class="video-label-item label-color-0" href="/cate/lol" data-pdt-ele="1">英雄联盟</a>
                            <a class="video-label-item label-color-5" href="/label/lmjy" data-pdt-ele="2">联盟精英</a>
                        </div>
                        </div>
                    </li> 
                </ul>
            </div>
            <div class="zhibo" style="display:none;">
                <ul id="later-play-list" class="video-list clearfix" data-total="2443">  
                    <li class="video-list-item1 video-no-tag  " data-pdt-block="a_jingji1-0" data-id="237908">
                        <a href="/237908" class="video-list-item-wrap" data-pdt-ele="0" data-id="237908">
                        <div class="video-cover">
                            <img class="video-img video-img-lazy" data-original="https://i.h2.pdim.gs/90/47d745af8e95ced57c22b83b5c464643/w338/h190.webp" alt="老刘：刘莱厄斯1V5Carry流！" src="https://i.h2.pdim.gs/90/05939e07a11f1fa12f2a3f2aef06ac6a/w338/h190.webp" style="display: block;">
                            <div class="video-overlay"></div>
                            <div class="video-play"></div>
                        
                        </div>
                        <div class="video-info">
                            <span class="video-title" title="老刘：刘莱厄斯1V5Carry流！">4老刘：刘莱厄斯1V5Carry流！</span>                              
                        </div>
                        </a>
                        <div class="video-label">
                        <div class="video-label-content">
                            <a class="video-label-item label-color-0" href="/cate/lol" data-pdt-ele="1">英雄联盟</a>
                            <a class="video-label-item label-color-5" href="/label/lmjy" data-pdt-ele="2">联盟精英</a>
                        </div>
                        </div>
                    </li> 
                </ul>
            </div> -->
        </div>
    </div>
    </div>

<script>
    $(function(){

        $("#btnzb li").on("click",'a',function(){
         // 设index为当前点击
         var num = $("#btnzb li a").index(this);
         var a = $(this).attr('abc');
         // 点击添加样式利用siblings清除其他兄弟节点样式
         $(this).addClass("selected").parent().siblings().find("a").removeClass("selected");
         // 同理显示与隐藏
        //  $(".zhibo:eq("+num+")").show().siblings().hide();
        $('.video-list-item1').css('display','none');
         $('.video-list [abc="'+a+'"]').css('display','block');
         if(a=="0"){
            $('.video-list-item1').css('display','block');
        }



       });



    });


</script>
 @endsection