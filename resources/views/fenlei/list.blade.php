@extends('base.base') 

@section('title','分类')

@section('content')
<link rel="stylesheet" href="/css/fenlei.css">
<script src="/js/bootstrap.min.js"></script>
    <div class="container-fluid">
    <div class="later-play-container">
          <!-- 列表头部信息 start -->
          <div class="list-header" data-pdt-block="a0">
            <i class="header-line"></i>
            <div class="list-title">{{$types->type_name}}</div>
            
          </div>
          <!-- 列表头部信息 end -->
          <div>
            <div class="zhibo" style="display:block;">
                <ul id="later-play-list" class="video-list clearfix" data-total="2443">  
                    @if($lives!=null)
                        @foreach($lives as $l)
                            @if($l->live_state==1)
                            <li class="video-list-item1 video-no-tag  " abc="{{$l->pid}}" data-pdt-block="a_jingji1-0" data-id="" style="display:block;">
                                <a href="/live-{{$l->id}}" class="video-list-item-wrap" data-pdt-ele="0" data-id="237908">
                                <div class="video-cover">
                                    <img class="video-img video-img-lazy" data-original="" alt="老刘：刘莱厄斯1V5Carry流！" src="/uploads/{{$l->cover}}" style="display: block;">
                                    <div class="video-overlay"></div>
                                    <div class="video-play"></div>
                                
                                </div>
                                <div class="video-info">
                                    <span class="video-title" title="">{{$l->live_name}}</span>                              
                                </div>
                                </a>
                                <div class="video-label">
                                <div class="video-label-content">
                                    <a class="video-label-item label-color-0" href="/ejflzq/{{$l->type_id}}/{{$l->pid}}" data-pdt-ele="1">{{$l->type_name}}</a>
                                    <span class="video-title" style="float:right;margin-top:5px;font-size: 12px;color:#999;" title="">{{$l->zb_name}}</span>

                                </div>
                                </div>
                            </li> 
                            @endif
                        @endforeach
                    @else
                    <h1 style="font-size:20px;font-weight:700;">暂无内容</h1>
                    @endif
               
                </ul>
            </div>
        </div>
    </div>
    </div>

 @endsection