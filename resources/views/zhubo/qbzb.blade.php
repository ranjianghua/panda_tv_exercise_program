@extends('base.base') 

@section('title','主播排行')

@section('content')
<link rel="stylesheet" href="/css/zbph.css">
<div id="main-container" class="rank-data-container pd-sc-content">
  <div class="data-title-container">
    <p>排行榜</p>
  </div>
  <div class="rank-table-container clearfix small-view">
    <!--  车站日榜start -->
    <div class="rank-column" data-pdt-block="r0" style="display: inline-block;">
      <div class="rank-table-content rank-table-station-day">
        <div class="rank-table-header">
          <p class="title">车站榜</p>
          <div class="question">
            <div class="question-hover-dialog">按照车票数排名</div>
          </div>
          <div class="exchange-tab">
            <a data-table="station-day" data-time="now" class="active">总榜</a>
           
          </div>
        </div>
        <div class="rank-table-body">
          <ul class="ps ps--active-y">
            @foreach($lives as $v)
            <li>
              <p class="rank"></p>
              <p class="anchor-detail">
                <a target="_blank" href="/337852" data-pdt-ele="0">
                  @if($v->zb_mdface!=null)
                  <img src="/uploads/{{$v->zb_mdface}}">
                  @else
                  <img src="image/default-face.jpg" alt="">
                  @endif
                  <span>{{$v->zb_name}}</span>
                    <span class="anchor-status live"></span>
                </a>
                  <span class="anchor-popular-detail">
                      收到{{$v->chepiao}}张车票
                  </span>
              </p>
            </li>
            @endforeach
                  
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
              <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;">
              </div>
            </div>
            <div class="ps__rail-y" style="top: 0px; height: 649px; right: 0px;">
              <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 64px;">
              </div>
            </div>
          </ul>
        </div>      
      </div>
    </div>

    <div class="rank-column" data-pdt-block="r0" style="display: inline-block;">
      <div class="rank-table-content rank-table-station-day">
        <div class="rank-table-header">
          <p class="title">土豪榜</p>
          <div class="question">
            <div class="question-hover-dialog">按照消费排名</div>
          </div>
          <div class="exchange-tab">
            <a data-table="station-day" data-time="now" class="active">总榜</a>
           
          </div>
        </div>
        <div class="rank-table-body">
          <ul class="ps ps--active-y">
            @foreach($tuhao as $v)
            <li>
              <p class="rank"></p>
              <p class="anchor-detail">
                <a target="_blank" href="javascript:;" data-pdt-ele="0">
                  @if($v->face!=null)
                  <img src="/uploads/{{$v->face}}">
                  @else
                  <img src="image/default-face.jpg" alt="">
                  @endif
                  <span>{{$v->nick_name}}</span>
                    <span class="anchor-status live"></span>
                </a>
                  <span class="anchor-popular-detail">
                      消费{{$v->total_money-$v->last_money}}猫币
                  </span>
              </p>
            </li>
            @endforeach
                  
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
              <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;">
              </div>
            </div>
            <div class="ps__rail-y" style="top: 0px; height: 649px; right: 0px;">
              <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 64px;">
              </div>
            </div>
          </ul>
        </div>      
      </div>
    </div>
    <div class="rank-column" data-pdt-block="r0" style="display: inline-block;">
      <div class="rank-table-content rank-table-station-day">
        <div class="rank-table-header">
          <p class="title">主播收入榜</p>
          <div class="question">
            <div class="question-hover-dialog">按照总收入排名</div>
          </div>
          <div class="exchange-tab">
            <a data-table="station-day" data-time="now" class="active">总榜</a>
           
          </div>
        </div>
        <div class="rank-table-body">
          <ul class="ps ps--active-y">
            @foreach($shouru as $v)
            <li>
              <p class="rank"></p>
              <p class="anchor-detail">
                <a target="_blank" href="javascript:;" data-pdt-ele="0">
                  @if($v->face!=null)
                  <img src="/uploads/{{$v->face}}">
                  @else
                  <img src="image/default-face.jpg" alt="">
                  @endif
                  <span>{{$v->nick_name}}</span>
                    <span class="anchor-status live"></span>
                </a>
                  <span class="anchor-popular-detail">
                      收入{{$v->zsr}}猫币
                  </span>
              </p>
            </li>
            @endforeach
                  
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
              <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;">
              </div>
            </div>
            <div class="ps__rail-y" style="top: 0px; height: 649px; right: 0px;">
              <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 64px;">
              </div>
            </div>
          </ul>
        </div>      
      </div>
    </div>
  </div>
</div>
          <!-- 车站周榜end -->
         


@endsection
