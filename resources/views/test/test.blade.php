@extends('base.base') 

@section('title','个人中心')

@section('content')
<link rel="stylesheet" href="/css/index.css">
<script src="/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="/cropper/cropper.min.css">
<script src="/cropper/cropper.min.js"></script>
<!-- 容器 -->
<div id="main-container" class="pd-sc-content" data-pdt-block="ps0">
    <div class="personal-container">
        <div class="tab-container clearfix">
            <a href="javascript:;" class="tab_button selected">基本资料</a>
            @if($lives!=null)
            <a href="javascript:;" class="tab_button">直播间管理</a>
            
            @else
            <!-- <a href="javascript:;" class="tab_button">我要当主播</a> -->
            @endif
            <!-- <a href="javascript:;" class="tab_button">私信设置</a>       -->
        </div>
        <div id="main-content" class="main-content">
            <!-- 设置基本资料 -->
            <div class="tabCon personal-setting-container" style="display:block;">
                <div class="personal-info">
                    <div class="personal-update">
                        <div class="avt">
                            @if(session('bgface')!=null)
                            <img src="/uploads/{{session('bgface')}}" alt="">
                            @else
                            <img src="/image/default-face.jpg" alt="">
                            
                            @endif
                        </div>
                        <div class="personal-update-avt">
                            <div class="webuploader-pick webuploader-container" id="filePicker">
                                <div class="webuploader-pick">
                                    <a href="javascript:void(0);" id="btn">设置头像</a>
                                </div>
                                <div>
                                <form action="/face" method="post" enctype="multipart/form-data" id="form1">
                                    {{csrf_field()}}
                                    <input type="file" id="up" name="face"  class="webuploader-element-invisible" multiple="multiple" accept="image/*" style="position: absolute; clip: rect(1px 1px 1px 1px);">
                                  
                                </form>
                                </div>
                            </div>
                            <div class="tips">JPG或PNG格式，最大3MB，不支持GIF</div>
                        </div>
                    </div>  
                    <div class="personal-info-content">
                        <div class="personal-list personal-name">
                        @if($errors->any())
                            <ul class="error">
                                @foreach($errors->all() as $e)
                                <li>{{$e}}</li>
                                @endforeach
                            </ul>
                        @endif
                            <label>昵称：</label>
                            <span class="nick_name">{{session('nick_name')}}</span>
                            <span class="modify-nickname xiugai">修改</span>
                            <div class="personal-modify-nickname" id="xiaoshi" style="display:none;">
                            <form action="/xgnc" method="get" id="form2">
                                <input type="text" class="personal-modify-nickname-val" name="nick_name" value="{{session('nick_name')}}">
                                <div class="personal-modify-nickname-cancel" id="quxiao">取消</div>
                                <div class="personal-modify-nickname-ok" id="xgnc">确定</div>
                                <div class="personal-modify-nickname-tip">
                                    昵称应为4-16个字符，不与其他用户重复

                                </div>
                            </form>
                            </div>
                        </div>
                        <div class="personal-list personal-level">
                            <label>等级：</label>
                            <span class="icon-level" id="dw-icon1" data-level="1" dj="{{session('duanwei')}}"></span>
                        </div>
                        <div class="personal-list personal-bamboo">
                            <label>竹子：</label><span class="bamboo">{{session('zhuzi')}}</span>
                        </div>
                        <div class="personal-list personal-maobi">
                            <label>猫币：</label><span class="maobi">{{session('maobi')}}</span>
                            <a href="javascript:;" onclick="zwfl()" target="_blank" data-pdt-ele="0">充值</a>
                            <span class="perosnal-charge-description">充10元 并没有什么卵用</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 设置基本资料 end -->
            <!-- 安全设置 -->
            @if($lives == null)
            <div class="tabCon personal-ruc-container" style="display:none;">
                <div class="personal-ruc-safe-container clearfix">
                </div>
                
            </div>
            @else
            <div class="tabCon personal-ruc-container personal-setting-container" style="display:none;">
                <div class="personal-ruc-safe-container clearfix"  style="padding:0;">
                    <div class="personal-info-content"  style="margin-top:0;">
                        <div class="personal-info" style="padding:0;">                 
                            <div class="personal-info-content" style="margin-bottom:20px;">
                                <div class="personal-list personal-name">
                                @if($errors->any())
                                    <ul class="error">
                                        @foreach($errors->all() as $e)
                                        <li>{{$e}}</li>
                                        @endforeach
                                    </ul>
                                @endif
                                    <label style="font-size:20px;">直播间名称：</label>
                                    <span style="font-size:18px;" class="nick_name">{{$lives->live_name}}</span>
                                    <span style="font-size:18px;" class="modify-nickname xiugai2">修改</span>
                                    <div class="personal-modify-nickname" id="xiaoshi2" style="display:none;">
                                    <form action="/xgfjm" method="get" id="form3">
                                        <input style="font-size:18px;width: 800px;" type="text" class="personal-modify-nickname-val" name="live_name" value="{{$lives->live_name}}">
                                        <div class="personal-modify-nickname-cancel" id="quxiao2">取消</div>
                                        <div class="personal-modify-nickname-ok" id="xgfjm">确定</div>
                                        <div class="personal-modify-nickname-tip">
                                            直播间名称应为2-16个字符，不与其他房间名称重复

                                        </div>
                                    </form>
                                    </div>
                                </div>                    
                            </div>
                        </div>
                        <img width="300" src="/uploads/{{ $lives->cover }}" alt="">
                        <form enctype="multipart/form-data" method="POST" action="/cover" id="form4">
                            {{ csrf_field() }}
                            <div class="form-div">
                                <a href="javascript:void(0);" id="btn20">修改直播间封面</a>
                                <input type="file" name="cover" id="btn21" style="position: absolute; clip: rect(1px 1px 1px 1px);">
                            </div>

                                <!-- 预览图片区域 -->
                            <div class="img-container">
                                <img id="pre_image" src="/uploads/{{ $lives->cover }}">			
                            </div>
                                <!-- 1个缩略图预览区域 -->
                            <div class="docs-preview clearfix">
                                <div class="img-preview preview-lg"></div>
                              
                            </div>
                                <!-- 保存裁切参数的四个框 -->
                            <input type="hidden" name="x">
                            <input type="hidden" name="y">
                            <input type="hidden" name="w">
                            <input type="hidden" name="h">
                            <div class="form-div">
                                <input type="botton" value="确认修改" class="cover-btn">
                            </div>
                        </form>
                        <div class="safe-items">
                            <a href="javascript:;" id="kaibo" class="bind-mobile-btn">@if($lives->live_state==0)我要开播@else我要下播@endif</a>
                        </div>
                </div>
                
            </div>
            @endif
            <!-- 安全设置 end -->
            <!-- 私信设置 -->
            <!-- <div class="tabCon personal-privatemsg-container" style="display:none;">
            <div class="personal-privatemsg-level">
                <div class="personal-privatemsg-levels-container">
                    <div class="personal-privatemsg-levels-title">设置是否接收私信：</div>
                    <div class="personal-privatemsg-levels-forbidall">
                        <div class="personal-privatemsg-levels-forbidall-img">
                        </div>
                        <div class="personal-privatemsg-levels-allowall-select-mask"></div>
                        <div class="personal-privatemsg-levels-forbidall-title">禁止接收</div>
                    </div>        
                </div>
            </div> -->
            <!-- 私信设置 end -->

        </div>
    </div>


</div>





<script type="text/javascript">
   //
	$("#kaibo").click(function(){

        $.ajax({
            type:"GET",
            url:"/kaibo/{{session('user_id')}}",
            dataType:"json",
            success:function(data){
                if(data==0){
                    $("#kaibo").text('我要下播');
                }else{
                    $("#kaibo").text('我要开播');
                }
            }
        });
    });
     

    $(function(){   
       $(".tab-container").on("click","a",function(){
         // 设index为当前点击
         var num = $(".tab-container a").index(this);
        
         // 点击添加样式利用siblings清除其他兄弟节点样式
         $(this).addClass("selected").siblings().removeClass("selected");
         // 同理显示与隐藏
         $(".tabCon:eq("+num+")").show().siblings().hide();
        
       });
    });

        $('#btn').click(function(){
            $('#up').click();
        });

        $('.xiugai').click(function(){
            $("#xiaoshi").css('display','block');
        });
        $('.xiugai2').click(function(){
            $("#xiaoshi2").css('display','block');
        });

        $("#quxiao").click(function(){
            $("#xiaoshi").css('display','none');
        });
        $("#quxiao2").click(function(){
            $("#xiaoshi2").css('display','none');
        });

        function duanwei1(){
		var duanwei = null;
		// duanwei = {{session('duanwei')}};
        var a = $("#dw-icon1").attr('dj');
		duanwei = a*1;
        console.log(a);
		if(duanwei!=null){
			var x = 0;
			var y = 0;
			switch(duanwei){
				case 1:x=-200;y=0;break;
				case 2:x=-150;y=0;break;
				case 3:x=-100;y=0;break;
				case 4:x=-50;y=0;break;
				case 5:x=-0;y=0;break;
				case 6:x=-200;y=-20;break;
				case 7:x=-150;y=-20;break;
				case 8:x=-100;y=-20;break;
				case 9:x=-50;y=-20;break;
				case 10:x=-0;y=-20;break;
				case 11:x=-200;y=-40;break;
				case 12:x=-150;y=-40;break;
				case 13:x=-100;y=-40;break;
				case 14:x=-50;y=-40;break;
				case 15:x=-0;y=-40;break;
				default:break;
                }
                $('#dw-icon1').css('background-position',x+'px '+y+'px');
            }
        }
	
	duanwei1();

    $(function() {

        $("#up").on("change", function() {
            $("#form1").submit();
        });

        $("#xgnc").on("click",function(){
            $("#form2").submit();
        });
        $("#xgfjm").on("click",function(){
            $("#form3").submit();
        });

        $("#btn20").on("click",function(){
            $("#btn21").click();
        });

        $(".cover-btn").on("click",function(){
            $("#form4").submit();
        });
    });
   
   



</script>
<script>
//获取裁切是的四个框
var x = $("input[name=x]");
var y = $("input[name=y]");
var w = $("input[name=w]");
var h = $("input[name=h]");
//获取图片
var preImg = $("#pre_image");
//选择图片时预览图片并调用cropper插件
$("input[name=cover]").change(function(){
	//先销毁，清除一下插件，否则连续调用插件时会失败
	preImg.cropper("destroy");
	//this.files[0]：获取当前图片并转成URL地址
	var url = getObjectUrl(this.files[0]);
	//设置url到预览上
	preImg.attr('src',url);
	//调出插件
	preImg.cropper({

		aspectRatio:2,  //裁切的框形状
		preview:'.img-preview', //显示预览的位置
		viewModel:3,  //显示模式
		//裁切时把参数保存到表单中
		crop:function(event){
			x.val(event.detail.x);
			y.val(event.detail.y);
			w.val(event.detail.width);
			h.val(event.detail.height);
		}

	});
});
// 预览时需要使用下面这个函数转换一下
function getObjectUrl(file) {
    var url = null;
    if (window.createObjectURL != undefined) {
        url = window.createObjectURL(file)
    } else if (window.URL != undefined) {
        url = window.URL.createObjectURL(file)
    } else if (window.webkitURL != undefined) {
        url = window.webkitURL.createObjectURL(file)
    }
    return url
}

</script>
 @endsection