@extends('base.base') 

@section('title','直播间')

@section('content')
<script src="https://g.alicdn.com/de/prismplayer/2.6.0/aliplayer-flash-min.js"></script>
<link rel="stylesheet" href="css/live.css">
<div class="room-content-box">
    <div class="content-header">
        <div class="content-header-img">
            <img src="uploads/{{$zb_arr['face']}}" alt="">
        </div>
        <h1 class="content-header-info">
            {{$room_arr->live_name}}
        </h1>
        <div class="content-zhubo-info">
            <span class="zb_name">
                {{$room_arr->zb_name}}
            </span>
            <span class="room_id">
                房间号：{{$room_id}}
            </span>
        </div>
        <div class="content-head-tool">
            <div class="tool-right">
                <div class="renshu">
                    {{$room_arr->viewer}}
                </div>
                <div class="dingyue" onclick="dingyue()">
                    {{$follow_type}}
                </div>
            </div>
        </div>
    </div>
    <div class="player-box1" id="player">
        @if($room_arr->live_state)
        <div id="J_prismPlayer">
            
        </div>
        @else
        <div class="buzaijia">
            你当前观看的主播不在家
        </div>
        @endif
    </div>
    <div class="room-foot-box">
        <ul class="gift">
            @foreach($gifts_arr as $v)
            <li>
                <a href="javascript:;"  onclick="gift({{$v->id}})">
                    <img src="{{$v->gift_img}}" alt="">
                </a>
            </li>
            @endforeach
            <li>
                <a href="javascript:;"  onclick="zanwu()">
                    <img src="image/zhuzi.png" alt="">
                </a>
            </li>
        </ul>
    </div>
    <div class="room-zbxq">
        <div class="zbxq-head"><h1 class="bt">直播详情</h1></div>
        <div class="zbxq-content">
            <p>主播很懒，什么也没写</p>
        </div>
    </div>
</div>
<div class="room-zhongjian"></div>
<div class="room-chat-box">
    <div class="chat-box-gonggao">
        <i></i>
        <em>直播公告</em>
        <span>这个主播很，懒什么都没写123123123123123123123</span>
    </div>
    <div class="chat-gift">
        
    </div>
    <div class="room-chat-container">
        <ul id="a1">
            <li>
                
            </li>
        </ul>
    </div>
    <div class="chat-input">
        <div class="chat-input-container">
             @if(session('user_id')==null)
            <div class="chat-kuang">
                <a href="javascript:;" class="chat-login" onclick="tanchuang('login')">登录</a>即可发送弹幕
            </div>
            <div class="room-chat-send">发送</div>
            @else
            <div class="chat-kuang">
                <textarea id="send_content" style="width: 100%;height: 100%;color:#333;line-height: 1.5em;" placeholder="跟主播聊聊天吧" maxlength="40"></textarea>
            </div>
            <div class="room-chat-send" style="background-color:#04c073;cursor: pointer;" onclick="send_content()">发送</div>
            @endif
        </div>
        
    </div>
</div>
<!-- <div  class="prism-player" id="J_prismPlayer" style="position: absolute"></div> -->
    <script>
        @if($room_arr->live_state)
        var player = new Aliplayer({
            id: 'J_prismPlayer',
            width: '100%',
            height:'100%',
            autoplay: false,
            //支持播放地址播放,此播放优先级最高
            source : 'rtmp://xx.x.x.x/live/{{$room_arr->id}}',
            isLive: true,
            autoplay: true,
            },function(player){
              console.log('播放器创建好了。')
        });
        @endif
        setInterval(function(){
                var width = $('#player').width();
                // console.log(width);
                var height = width*9/16;
                $('#player').height(height);
        },100);

        function zanwu(){
            alert('暂无此功能');
        }

        setInterval(function(){
            var danmu = document.getElementsByClassName('danmu');
            for(var i=0;i<danmu.length;i++){
                var left = parseFloat(danmu[i].style.left);
                // console.log(left);

                if(left <= -30){
                    danmu[i].parentNode.removeChild(danmu[i]);
                    i-=1;
                    danmu = document.getElementsByClassName('danmu');
                }else{
                    left -= 0.4;
                    danmu[i].style.left = left+'%';
                }
            }
        },50);

        function gift(id){
            if(id!='zhuzi'){
                $.get('{{route('gift',['id'=>$room_id])}}',
                    {
                    r:Math.random(),
                    'gift_id':id,
                    'zb_id':{{$room_arr->zb_id}},
                    'room_id':{{$room_id}}
                    },function(data){
                        if(data.type=='gift'){
                            // console.log(data);
                            $('.hb-maobi span').html($('.hb-maobi span').html()*1-data.total_price);
                            ws.send(JSON.stringify(data));  
                        }else if(data.type==0){
                            alert(data.error);
                        }
                });
            }else{

            }
            
        }

        function send_content(){
            var content = $("#send_content").val();
            if(content==""){
                alert("内容不可为空");
                $("#send_content").val("");
                return;
            }
            var a = {'type':'message','user_type':'{{$user_type}}','uname':'{{session('user_id')!=null ? session('nick_name') : 'none'}}','room_id':'{{$room_id}}','content':content};
            ws.send(JSON.stringify(a));
            $("#send_content").val("");
        }

        var ws = new WebSocket("ws://{{$_SERVER['SERVER_NAME']}}:8123");
        ws.onopen = function() {
            var a = {'type':'connect','user_type':'{{$user_type}}','uname':'{{session('user_id')!=null ? session('nick_name') : 'none'}}','room_id':'{{$room_id}}','content':'连接成功'};
            ws.send(JSON.stringify(a));
        };
        // function b1(){
        //     var x = document.getElementById('t1').value;
        //     if(x==""){
        //         alert('内容不可为空');
        //     }else{
        //         var a = {'type':'message','content':x};
        //         ws.send(JSON.stringify(a));
        //     }
            
        // }
        // var top = 0;
        ws.onmessage = function(e) {
            // document.getElementById('a1').innerHTML+=e.data;
            // $('#a1').scrollTop( $('#a1')[0].scrollHeight );
            // alert("收到服务端的消息：" + e.data);
            var a = JSON.parse(e.data);
            console.log(a.content);
            
            var user_type = "<span class='green'>";
            if(a.user_type=="主播"){
                user_type = "<span class='zb-color'>[主播]";
            }else if(a.user_type=="房管"){
                user_type = "<span class='fg-color'>[房管]";
            }
            var height = $('.player-box1').height()-20;
            var top = Math.floor(Math.random()*height);
            if(a.type=="connect"){
                if(a.uname!="none"){
                    $('#a1').append('<li>'+user_type+a.uname+'</span>&nbsp;进入房间！</li>');
                }
            }else if(a.type=="message"){
                if($('#a1').children().length>100){
                    $('#a1').children().first().remove();
                }
                // console.log(a.content.replace(/</g,"&lt;").replace(/>/g,'&gt;'));
                $('#a1').append('<li>'+user_type+a.uname+'</span>&nbsp;说:'+a.content+'</li>');
                $('#a1').scrollTop( $('#a1')[0].scrollHeight );
                
                $('.player-box1').append('<p class="danmu" style="top:'+top+'px;left:100%;">'+a.content+'</p>');
            }else if(a.type=="gift"){
                if($('.chat-gift').children().length>100){
                    $('.chat-gift').children().first().remove();
                }
                $('.chat-gift').append('<li><span class="green">'+a.user_name+'</span>&nbsp;送给<span class="green">'+a.zb_name+'</span><span class="fg-color">'+a.gift_num+'</span>个<img src="'+a.gift_img+'" alt="" />'+a.gift_name+'</li>');
                $('.chat-gift').scrollTop( $('.chat-gift')[0].scrollHeight );
                $('.player-box1').append('<p class="danmu" style="top:'+top+'px;left:100%;"><span class="green">'+a.user_name+'</span>&nbsp;送给主播<span class="green">'+a.gift_num+'</span>个<span class="green">'+a.gift_name+'</span><img src="'+a.gift_img+'" alt="" /></p>')
            }
            

        };
        document.onkeydown = function (event) {
            var e = event || window.event;
            if (e && e.keyCode == 13) { //回车键的键值为13
                send_content(); //调用登录按钮的登录事件
            }
        };
        function dingyue(){
            $.get('{{route('dingyue',['id'=>$room_id])}}',{r:Math.random()},function(data){
                data = JSON.parse(data);
                if(data.type==1){
                    $(".dingyue").html('取消订阅');
                    $(".renshu").html(data.viewer);
                }else{
                    $(".renshu").html(data.viewer);
                    $(".dingyue").html('订阅');
                }
            });
        }
    </script>
@endsection