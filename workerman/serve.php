<?php 
	use Workerman\Worker;
	use Workerman\Protocols\Http;
	require_once 'Workerman/Autoloader.php';
	// define('HEARTBEAT_TIME', 25);
	$global_uid = 0;
	$clients = [];
	$connection->onWebSocketConnect = function($connection,$http_header){
    	global $clients;
    	$clients[$connection->uid] = $_GET['session_name'];
    	// var_dump($clients[$connection->uid]);
    };
	// 当客户端连上来时分配uid，并保存连接，并通知所有客户端
	function handle_connection($connection)
	{
		// var_dump($_GET);

	    global $text_worker, $global_uid , $clients;
	    // 为这个连接分配一个uid
	    $connection->uid = ++$global_uid;
	    // $clients[$connection->uid]['uname'] = $_GET['session_name'];
	}

	// 当客户端发送消息过来时，转发给所有人
	function handle_message($connection, $data)
	{	
	    global $text_worker ,  $clients;
		$obj = json_decode($data);
		if($obj->type=="connect"){
			$clients[$connection->uid]['uname'] = $obj->uname;
			$clients[$connection->uid]['room_id'] = $obj->room_id;
			foreach($text_worker->connections as $conn)
		    {
		    	if($clients[$conn->uid]['room_id']==$obj->room_id){
		    		$conn->send($data);
		    	}
		    }
		}else if($obj->type=="message"){
			$obj->content = str_replace("<", "&lt;", $obj->content);
			$obj->content = str_replace(">", "&gt;", $obj->content);
			// var_dump($obj);
			foreach($text_worker->connections as $conn)
		    {
		    	if($clients[$conn->uid]['room_id']==$obj->room_id){
		    		$conn->send(json_encode($obj));
		    	}
		    }
		}else if($obj->type=="gift"){
			foreach($text_worker->connections as $conn)
		    {
		    	if($clients[$conn->uid]['room_id']==$obj->room_id){
		    		$conn->send(json_encode($obj));
		    	}
		    }
		}
	    
	}

	// 当客户端断开时，广播给所有客户端
	function handle_close($connection)
	{
	    global $text_worker , $clients;
	    foreach($text_worker->connections as $conn)
	    {
	        $conn->send("{$connection->uid}号用户 已断开连接<br>");
	    }
	}

	// 创建一个文本协议的Worker监听8123接口
	$text_worker = new Worker("websocket://0.0.0.0:8123");

	// 只启动1个进程，这样方便客户端之间传输数据
	$text_worker->count = 1;

	$text_worker->onConnect = 'handle_connection';
	$text_worker->onMessage = 'handle_message';
	$text_worker->onClose = 'handle_close';

	Worker::runAll();