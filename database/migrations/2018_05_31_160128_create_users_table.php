<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nick_name')->nullable()->comment('昵称');
            $table->string('password')->comment('账号密码');
            $table->string('mobile')->comment('手机号码');
            $table->string('email')->nullable()->comment('邮箱');
            $table->tinyInteger('message')->default(0)->comment('私信开关');
            $table->tinyInteger('user_type')->default(1)->comment('用户类型');
            $table->unsignedInteger('total_money')->default(0)->comment('总充值金额');
            $table->unsignedInteger('last_money')->default(0)->comment('余额');
            $table->string('face')->nullable()->comment('头像');
            $table->timestamps();


            $table->engine='innodb';
            $table->comment='用户表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
