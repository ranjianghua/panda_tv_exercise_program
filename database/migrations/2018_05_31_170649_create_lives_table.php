<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lives', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->string('live_name')->comment('直播房间名');
            $table->unsignedInteger('zb_id')->comment('主播id');
            $table->string('zb_name')->comment('主播名称');
            $table->enum('zb_gender',['man','woman','wz'])->default('wz')->comment('性别');
            $table->unsignedInteger('viewer')->comment('观众数');
            $table->unsignedTinyInteger('live_state')->default('0')->comment('直播状态:0为停播，1为正在直播');
            $table->unsignedTinyInteger('type_id')->index()->comment('直播类型id');
            $table->timestamps();

            $table->engine = 'innodb';
            $table->comment = '直播表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lives');
    }
}
