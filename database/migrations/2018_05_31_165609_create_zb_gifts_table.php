<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZbGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zb_gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index()->comment('用户ID');
            $table->unsignedInteger('zb_id')->index()->comment('主播ID');
            $table->unsignedInteger('gift_id')->index()->comment('礼物ID');
            $table->unsignedTinyInteger('gift_num')->comment('礼物数量');
            $table->string('gift_name')->comment('礼物名称');
            $table->unsignedInteger('total_price')->comment('礼物总价');
            $table->string('gift_img')->comment('礼物图片');
            $table->timestamps();

            $table->engine = 'innodb';
            $table->comment = '主播礼物表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zb_gifts');
    }
}
