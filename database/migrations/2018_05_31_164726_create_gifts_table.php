<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {

            $table->increments('id');
            $table->string('gift_name')->comment('礼物名称');
            $table->unsignedInteger('gift_price')->comment('礼物价格');
            $table->string('gift_img')->comment('礼物图片');

            $table->engine = 'innodb';
            $table->comment = '礼物信息表';
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
