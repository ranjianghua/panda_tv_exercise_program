<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiveAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('live_admins', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('live_id')->idnex()->comment('直播房间id');
            $table->unsignedInteger('user_id')->index()->comment('用户id');
            $table->unsignedTinyInteger('part')->default(0)->comment('房管权限：1为踢人 2为禁言 0为所有权限 默认为0');

            $table->engine = 'innodb';
            $table->comment = '房间管理员表';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('live_admins');
    }
}
