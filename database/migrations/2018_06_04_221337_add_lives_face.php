<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLivesFace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lives', function (Blueprint $table) {
            $table->string('zb_face')->comment('头像');
            $table->string('zb_bgface')->comment('大缩略图');
            $table->string('zb_mdface')->comment('小缩略图');
            $table->string('pid')->comment('直播类型父id');
            $table->string('type_name')->comment('直播类型名称');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lives', function (Blueprint $table) {
            $table->dropcolumn('zb_face');
            $table->dropcolumn('zb_bgface');
            $table->dropcolumn('zb_mdface');
            $table->dropcolumn('type_name');
            $table->dropcolumn('pid');
        });
    }
}
