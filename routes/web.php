<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/********登录模块S*******/
//登录
Route::post('/login','LoginController@login')->name('login');
//退出登录
Route::get('/logout','LoginController@logout')->name('logout');
;
//直播间模块
Route::get('/live-{id}','LiveController@live')->name('live');

//订阅和取消订阅路由
Route::get('/dingyue/{id}','LiveController@dingyue')->name('dingyue');
//礼物
Route::get('/gift/{id}','LiveController@gift')->name('gift');

/********登录模块E*******/

//全部分类
Route::get('/','TypeController@alltype')->name('alltype');
//全部直播
Route::get('/fenlei','TypeController@fenlei')->name('fenlei');
//主播排行
Route::get('/zbph','LiveController@zbph')->name('zbph');
//注册发送短信验证码
Route::get('/sendmobilecode', 'UserController@sendmobilecode')->name('ajax-send-modbile-code');
//注册表单处理
Route::post('/regist','UserController@doregist')->name('doregist');

Route::middleware('login')->group(function(){


//个人中心
Route::get('/grzx','UserController@grzx')->name('grzx');

//修改头像
Route::post('/face','UserController@face')->name('face');
//修改昵称
Route::get('/xgnc','UserController@xgnc')->name('xgnc');

//修改房间名称
Route::get('/xgfjm','UserController@xgfjm')->name('xgfjm');

//修改直播间图片
Route::post('/cover','LiveController@cover')->name('cover');
//直播状态更新
Route::get('/kaibo/{id}','LiveController@kaibo')->name('kaibo');



});

//21:30 热门竞技分类 娱乐联盟分类 网友专区分类
Route::get('/flzq/{id}','TypeController@flzq')->name('flzq');

//22:37 二级分类
Route::get('/ejflzq/{id}/{pid}','TypeController@ejflzq')->name('ejflzq');