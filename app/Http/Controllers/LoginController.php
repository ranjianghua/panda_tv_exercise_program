<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\Models\User;

class LoginController extends Controller
{
    function login(Request $req){
        $arr = User::where('mobile',$req->mobile)->where('password',$req->password)->first();
        // dd($arr);
        if($arr==null){
            // dd();
             return back()->with('abc','123')->withErrors(['账号或者密码不正确']);
        }else{
            session([
                'user_id'=>$arr->id,
                'face'=>$arr->face,
                'mdface'=>$arr->mdface,
                'bgface'=>$arr->bgface,
                'nick_name'=>$arr->nick_name,
                'maobi'=>$arr->last_money,
                'zhuzi'=>$arr->zhuzi,
                'user_type'=>$arr->user_type,
                'duanwei'=>$arr->duanwei,
            ]);
            return back();
        }
    }

    function logout(Request $req){
        $req->session()->flush();
        return back();
    }
}
