<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Type;
use App\Models\Live;
use Illuminate\Http\Request;
use App\Http\Requests\RegistRequest;
use Storage;
use Image;
use Hash;
use Flc\Dysms\Client;
use Flc\Dysms\Request\SendSms;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    //发送短信验证码
    public function sendmobilecode(Request $req){
       //生成6位的随机数
       $code =  rand(100000,999999);
       //缓存时的名字
       $name = 'code_'.$req->mobile; 
       //把随机数缓存起来（1分钟）
       Cache::put($name,$code,1);

       //向消息队列中发信息
       // Redis::lpush('sms_list',$req->mobile.'-'.$code);
       // 发短信
       $config = [
           'accessKeyId'    => 'LTAICz56hWdfWYgf',
           'accessKeySecret' => 'pKFewJgeW0sWDD8ejg1E1ZkxENx02r',
       ];
       $client  = new Client($config);
       $sendSms = new SendSms;
       $sendSms->setPhoneNumbers($req->mobile);
       $sendSms->setSignName('订餐系统注册验证码');
       $sendSms->setTemplateCode('SMS_130035028');
       $sendSms->setTemplateParam(['code' => $code]);
       // 发送
       // $client->execute($sendSms);
       
      
       //执行发送
       print_r($client->execute($sendSms));

    }

    public function grzx(){
        $menu = '';
        $type = Type::get();
        $lives = Live::where('zb_id',session('user_id'))->first();
        // dd($lives);
        // $user = User::find(session('user_id'));
        return view('test.test',[
            'menu'=>$menu,
            'type'=>$type,
            // 'user'=>$user,
            'lives'=>$lives,
            
        ]);
    }

    public function xgnc(Request $req)
    {
        $user = User::find(session('user_id'));
        
        $sfcz = User::where('nick_name',$req->nick_name)->first();

        if($sfcz!=null){
            return back()->withErrors(['昵称已存在']);
        }
        else
        {
            
            $user->nick_name = $req->nick_name;
            $user->save();
            session([
                'nick_name'=>$user->nick_name,
            ]);
        }

        return redirect()->route('grzx');
        
    }

    public function xgfjm(Request $req)
    {
        $lives = Live::where('zb_id',session('user_id'))->first();
        // dd($lives);
        
        $sfcz = Live::where('live_name',$req->live_name)->first();

        if($sfcz!=null){
            return back()->withErrors(['房间名已存在']);
        }
        else
        {
            
            $lives->live_name = $req->live_name;
            $lives->save();
        }

        return redirect()->route('grzx');
        
    }

    public function face(Request $req)
    {
       
        // dd($req);
        if($req->has('face')&&$req->face->isValid())
        {               
            //先获取当前的日期
            $date = date('Ymd');
            //保存原图片
            $oriImg = $req->face->store('face/'.$date);
            // dd($oriImg);
            //获取上传图片的原始路径
            
            $path = $req->face->path();
           
            
            // dd($ext);
            //创建图片对象
            $img = Image::make($path);
            // dd($img);
            //裁剪图片
            // $img->crop((int)$req->w,(int)$req->h,(int)$req->x,(int)$req->y);
            
            //拼出这个缩略图的名字
            $bgname = str_replace('face/'.$date.'/','face/'.$date.'/bg_',$oriImg);
            $img->resize(120,120);
            $img->save('./uploads/'.$bgname);

            $mdname = str_replace('face/'.$date.'/','face/'.$date.'/md_',$oriImg);
            $img->resize(68,68);
            $img->save('./uploads/'.$mdname);

            //删除原头像
            $user = User::find( session('user_id') );
            $live = Live::where('zb_id',session('user_id'))->first();
            Storage::delete($user->face);
            Storage::delete($user->bgface);
            Storage::delete($user->mdface);
            //更新头像
            $user->face = $oriImg;
            $user->bgface = $bgname;
            $user->mdface = $mdname;
            if($live!=null){
                $live->zb_face = $oriImg;
                $live->zb_bgface = $bgname;
                $live->zb_mdface = $mdname;
                $live->save();
            }
            
            $user->save();
            
            // dd($user);
            session([
                'bgface'=>$bgname,
                'mdface'=>$mdname,
            ]);

            return redirect()->route('grzx');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doregist(RegistRequest $req)
    {
         // 拼出缓存的名字
         $name = 'code-'.$req->mobile;
         //再根据名字取出验证码
         $code = Cache::get($name);
         
         if(!$code || $code != $req->mobile_code)
         {
             return back()->with('aaa','123')->withErrors(['mobile_code'=>'验证码错误！']);
         }
        // dd($req);
         // 密码加密
         $password = $req->password;
         // 创建一个user对象
         $user = new User;
        
         $user->mobile = $req->mobile;
         $user->password = $password;
         $user->nick_name = $req->nick_name;
         $user->last_money = 100000;
         $user->total_money = 100000;
         $user->duanwei = 1;

 
 
         // 保存到表中 dd($user);
         $user->save();
        $arr = User::orderBy('id','desc')->first();
        session([
                'user_id'=>$arr->id,
                'face'=>$arr->face,
                'mdface'=>$arr->mdface,
                'bgface'=>$arr->bgface,
                'nick_name'=>$arr->nick_name,
                'maobi'=>$arr->last_money,
                'zhuzi'=>$arr->zhuzi,
                'duanwei'=>$arr->duanwei,
                'user_type'=>$arr->user_type,
            ]);
         // 跳转到 登录页
         return redirect()->route('alltype')->with('aaa','123');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
