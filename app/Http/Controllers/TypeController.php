<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\Live;
use Illuminate\Http\Request;
use Storage;
use DB;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function fenlei()
    {    
        $menu = 'qbzb';

        $lives = Live::get();
        // dd($lives);
        $type = Type::get();

        return view('fenlei.fenlei',[
            'menu'=>$menu,
            'type'=>$type,
            'lives'=>$lives,
            
        ]);
    }

    //左侧公共部分全部分类的一级分类专区
    public function flzq($id)
    {    
        $menu = 'qbzb';

        $lives = Live::where('pid',$id)->where('live_state',1)->get();
        // dd($lives);
        $type = Type::get();
        $types = Type::find($id);
        
        // dd( $type );
        return view('fenlei.list',[
            'menu'=>$menu,
            'type'=>$type,
            'types'=>$types,
            'lives'=>$lives,
            
        ]);
    }
     //左侧公共部分全部分类的二级分类专区
    public function ejflzq($id,$pid)
    {    
        $menu = 'qbzb';

        $lives = Live::where('type_id',$id)->where('live_state',1)->get();
        // dd($lives);
        $type = Type::get();
        $types = Type::find($id);
        $types2 = Type::find($pid);
        // dd( $type );
        return view('fenlei.ejlist',[
            'menu'=>$menu,
            'type'=>$type,
            'types'=>$types,
            'types2'=>$types2,
            'lives'=>$lives,
            
        ]);
    }

    public function alltype()
    {    

        $menu = 'qbfl';
        $type = Type::get();
        // dd($type);
       
        return view('fenlei.alltype',[
            'menu'=>$menu,
            'type'=>$type,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        //
    }
}
