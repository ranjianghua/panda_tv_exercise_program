<?php

namespace App\Http\Controllers;

use App\Models\Live;
use App\Models\User;
use DB;
use Cache;
use App\Models\Type;
use Storage;
use Image;
use Illuminate\Http\Request;

class LiveController extends Controller
{
    public function live($id){
        $data['menu']=null;
        $data['type'] = Type::get();
        $data['gifts_arr'] = Cache::remember('gifts', 3600, function() {
	       	return DB::table('gifts')->get();
	   	});

        $data['room_arr']=Live::find($id);
        if($data['room_arr']==null){
        	return "<script>alert('该房间不存在');history.go(-1)</script>";
        }
    	$x = DB::table('follows')->where('follow_id',$id)->where('user_id',session('user_id'))->first();
    	if($x==null){
    		$data['follow_type'] = '订阅';
    	}else{
    		$data['follow_type'] = '取消订阅';
    	}
        $data['zb_arr']=User::where('id',$data['room_arr']->zb_id)->first();
        $data['room_id'] = $id;
        if(session('user_id')==$data['room_arr']->zb_id){
        	$data['user_type'] = '主播';
        }else{
        	$x = DB::table('live_admins')->where('user_id',session('user_id'))->where('live_id',$id)->first();
        	if($x==null){
        		$data['user_type'] = '普通用户';
        	}else{
        		$data['user_type'] = '房管';
        	}
        }
        // dd($_SERVER);
        return view('live.live',$data);
    }

    public function dingyue($id){
    	$x = DB::table('follows')->where('follow_id',$id)->where('user_id',session('user_id'))->first();
    	if($x==null){
    		DB::table('follows')->insert(['user_id'=>session('user_id'),'follow_id'=>$id]);
    		$y = 1;
    	}else{
    		DB::table('follows')->where('follow_id',$id)->where('user_id',session('user_id'))->delete();
    		$y = 0;
    	}
    	$x = DB::table('follows')->where('follow_id',$id)->where('user_id',session('user_id'))->count();
    	Live::where('id',$id)->update(['viewer'=>$x]);
    	return json_encode(['type'=>$y,'viewer'=>$x]);
    }

    public function gift(Request $req){
    	$gift = DB::table('gifts')->where('id',$req->gift_id)->first();
    	$money = User::where('id',session('user_id'))->value('last_money');
    	if($money<$gift->gift_price){
    		return ['type'=>0,'error'=>'余额不足'];
    	}else{
    		User::where('id',session('user_id'))->decrement('last_money',$gift->gift_price);
    		$data = [
    				'user_id'=>session('user_id'),
    				'zb_id'=>$req->zb_id,
    				'gift_id'=>$gift->id,
    				'gift_num'=>1,
    				'gift_name'=>$gift->gift_name,
    				'total_price'=>$gift->gift_price,
    				'gift_img'=>$gift->gift_img,
    			];
    		DB::table('zb_gifts')->insert($data);
            session(['maobi'=>session('maobi')-$gift->gift_price]);
    		$data['user_name'] = session('nick_name');
    		$data['zb_name'] = User::where('id',$req->zb_id)->value('nick_name');
    		$data['type'] = 'gift';
    		$data['room_id'] = $req->room_id;
    		return $data;
    	}
    	// dd($gift);
    }
	
	public function cover(Request $req){

        if($req->has('cover')&&$req->cover->isValid())
        {               
            //先获取当前的日期
            $date = date('Ymd');
            //保存原图片
            $oriImg = $req->cover->store('cover');
            // dd($oriImg);
            //获取上传图片的原始路径
            $path = $req->cover->path();
            
            //创建图片对象
            $img = Image::make($path);
            //裁剪图片
            $img->crop((int)$req->w,(int)$req->h,(int)$req->x,(int)$req->y);
            
            //拼出这个缩略图的名字
            
            $img->resize(300,168);
            $img->save('./uploads/'.$oriImg);
            //删除原头像
            $live = Live::where('zb_id',session('user_id'))->first();
            Storage::delete($live->cover);
            //更新头像
            $live->cover = $oriImg;

            $live->save();
            // dd($user);
            session([
                'cover'=>$oriImg,
            ]);

            return redirect()->route('grzx');
        }
    }

    public function kaibo($id){
        $live = Live::where('zb_id',$id)->first();
        $num = $live->live_state;
        $num = (int)$num;
        if($num==1){
            $live->live_state = 0;
            $live->save();
            return 1;
        }
        else{
            $live->live_state = 1;
            $live->save();
            return 0;
        }
    }
    public function zbph(){
        $menu = 'zbph';
        $type = Type::get();
        $lives = Cache::remember('lives_paihang', 5, function() {
            return Live::orderBy('chepiao','desc')->get();
        });
        $tuhao = Cache::remember('tuhao_paihang', 5, function() {
            return User::select('*',DB::raw('(total_money - last_money) xf'))->orderBy('xf','desc')->get();
        });
        $shouru = Cache::remember('shouru_paihang', 5, function() {
            return DB::table('zb_gifts')->select('zb_id','users.nick_name','users.face',DB::raw('sum(total_price) zsr'))->groupBy('zb_id')->join('users','users.id','=','zb_gifts.zb_id')->orderBy('zsr','desc')->get();
        });
        // $shouru = DB::table('zb_gifts')->select('zb_id','users.nick_name','users.face',DB::raw('sum(total_price) zsr'))->groupBy('zb_id')->join('users','users.id','=','zb_gifts.zb_id')->orderBy('zsr','desc')->get();
        return view('zhubo.qbzb',[
            'menu'=>$menu,
            'type'=>$type,
            'lives'=>$lives,
            'tuhao'=>$tuhao,
            'shouru'=>$shouru,
        ]);
    }
}
