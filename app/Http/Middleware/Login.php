<?php

namespace App\Http\Middleware;

use Closure;

class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   

        /*在访问网站之前先执行代码*/
        
        //如果未登录就跳转到登录界面
        if(!session('user_id'))
        {   
            //如果是ajax的请求，返回json数据
            if($request->ajax())
            {
                return response([
                    'errno'=>999,
                    'errmsg'=>'必须先登录！',
                ]);
            }
            return back()->withInput()->withErrors('请登录');
            // return redirect()->route('alltype');
        }


        return $next($request);

    }
}
