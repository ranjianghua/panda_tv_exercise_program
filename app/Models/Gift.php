<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    protected $fillable = ['gift_name','gift_price','gift_img'];
    public $timestamps = false;
}
